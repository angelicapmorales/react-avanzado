import { Link } from "react-router-dom";

export function Navbar() {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            Portada
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              {/* <Link className="nav-link active" aria-current="page" href="#">
                Home
              </Link> */}
              <Link className="nav-link" to="/Login">
                Login
              </Link>
              <Link className="nav-link" to="/Contacto">
                Contacto
              </Link>
              <Link className="nav-link" to="/Info">
                Informacion
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}
