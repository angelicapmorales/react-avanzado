import { Field, Form, Formik } from "formik";
import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";

const LoginForm = ({ onSubmit, innerRef }) => {
  const inicial_data = {
    user_name: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  const context = useContext(ThemeContext);
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";

  return (
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef={innerRef}>
      {({}) => {
        return (
          <Form>
            <div className={text_class}>
              <div className="form-group">
                <label htmlFor="username">Nombre de usuario</label>
                <Field
                  className="form-control"
                  id="username"
                  type="text"
                  name="user_name"
                  placeholder="Ingrese su nombre de usuario"
                />
              </div>

              <div className="form-group py-4">
                <label htmlFor="pass">Contraseña</label>
                <Field
                  className="form-control"
                  id="pass"
                  type="password"
                  name="password"
                  placeholder="Ingrese su contraseña"
                />
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default LoginForm;
