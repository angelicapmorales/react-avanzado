import { Field, Form, Formik } from "formik";
import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";

const ContactoForm = ({ onSubmit, innerRef }) => {
  const inicial_data = {
    nombre: "",
    email: "",
    telefono: "",
    asunto: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  const context = useContext(ThemeContext);
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";

  return (
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef={innerRef}>
      {({}) => {
        return (
          <Form>
            <div className={text_class}>
              <div className="form-group">
                <label htmlFor="nombre">Nombre de usuario</label>
                <Field
                  className="form-control"
                  id="nombre"
                  type="text"
                  name="nombre"
                  placeholder="Ingrese su nombre completo"
                />
              </div>
              <div className="form-group py-4">
                <label htmlFor="email">Email</label>
                <Field
                  className="form-control"
                  id="email"
                  type="email"
                  name="email"
                  placeholder="example@gmail.com"
                />
              </div>
              <div className="form-group py-4">
                <label htmlFor="telefono">Telefono</label>
                <Field
                  className="form-control"
                  id="telefono"
                  type="text"
                  name="telefono"
                  placeholder="Ingrese su numero telefonico"
                />
              </div>
              <div className="form-group py-4">
                <label htmlFor="asunto">Asunto</label>
                <Field
                  className="form-control"
                  id="asunto"
                  type="text"
                  as="textarea"
                  name="asunto"
                  placeholder="Ingrese asunto"
                />
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default ContactoForm;
