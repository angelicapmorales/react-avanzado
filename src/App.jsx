import "./App.css";
import { Navbar } from "./components/navbar";
import Login from "./pages/login";
import { Routes, Route, Link } from "react-router-dom";
import { Portada } from "./pages/portada";
import { Info } from "./pages/info";
import Contacto from "./pages/contacto";
import ThemeProvider from "./utils/theme.context";

function App() {
  return (
    <div className="App">
      <Navbar />
      <ThemeProvider _theme="light">
        <Routes>
          <Route path="/" element={<Portada />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/Contacto" element={<Contacto />} />
          <Route path="/Info" element={<Info />} />
        </Routes>
      </ThemeProvider>
    </div>
  );
}

export default App;
