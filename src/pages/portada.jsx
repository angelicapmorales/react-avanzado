import React from "react";

export function Portada() {
  return (
    <div className="px-5">
      <div className="card mb-3">
        <div className="card-body">
          <h5 className="card-title">Portada</h5>
          <p className="card-text">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed
            voluptatibus iste rem, aspernatur quisquam nemo? Culpa quis, error
            harum maxime, explicabo natus dolorum dolores fuga repellendus quos
            distinctio neque voluptatum? Lorem ipsum dolor sit amet consectetur
            adipisicing elit. In adipisci officia provident magni facilis
            assumenda, quasi perspiciatis eligendi quae nemo aspernatur! Saepe
            perspiciatis, quia rerum quasi voluptate provident excepturi
            voluptates? Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Omnis dicta voluptate, cum tempore voluptatum soluta eum odit
            nesciunt voluptas eveniet eligendi provident adipisci officiis
            corrupti. Sunt vero sit nobis veritatis? Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Laudantium qui neque quia culpa
            pariatur quis porro quod. Facere possimus eum illum? Quibusdam
            cumque labore saepe quia, explicabo non asperiores repudiandae.
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed
            voluptatibus iste rem, aspernatur quisquam nemo? Culpa quis, error
            harum maxime, explicabo natus dolorum dolores fuga repellendus quos
            distinctio neque voluptatum? Lorem ipsum dolor sit amet consectetur
            adipisicing elit. In adipisci officia provident magni facilis
            assumenda, quasi perspiciatis eligendi quae nemo aspernatur! Saepe
            perspiciatis, quia rerum quasi voluptate provident excepturi
            voluptates? Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Omnis dicta voluptate, cum tempore voluptatum soluta eum odit
            nesciunt voluptas eveniet eligendi provident adipisci officiis
            corrupti. Sunt vero sit nobis veritatis? Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Laudantium qui neque quia culpa
            pariatur quis porro quod. Facere possimus eum illum? Quibusdam
            cumque labore saepe quia, explicabo non asperiores repudiandae.
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed
            voluptatibus iste rem, aspernatur quisquam nemo? Culpa quis, error
            harum maxime, explicabo natus dolorum dolores fuga repellendus quos
            distinctio neque voluptatum? Lorem ipsum dolor sit amet consectetur
            adipisicing elit. In adipisci officia provident magni facilis
            assumenda, quasi perspiciatis eligendi quae nemo aspernatur! Saepe
            perspiciatis, quia rerum quasi voluptate provident excepturi
            voluptates? Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Omnis dicta voluptate, cum tempore voluptatum soluta eum odit
            nesciunt voluptas eveniet eligendi provident adipisci officiis
            corrupti. Sunt vero sit nobis veritatis? Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Laudantium qui neque quia culpa
            pariatur quis porro quod. Facere possimus eum illum? Quibusdam
            cumque labore saepe quia, explicabo non asperiores repudiandae.
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed
            voluptatibus iste rem, aspernatur quisquam nemo? Culpa quis, error
            harum maxime, explicabo natus dolorum dolores fuga repellendus quos
            distinctio neque voluptatum? Lorem ipsum dolor sit amet consectetur
            adipisicing elit. In adipisci officia provident magni facilis
            assumenda, quasi perspiciatis eligendi quae nemo aspernatur! Saepe
            perspiciatis, quia rerum quasi voluptate provident excepturi
            voluptates? Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Omnis dicta voluptate, cum tempore voluptatum soluta eum odit
            nesciunt voluptas eveniet eligendi provident adipisci officiis
            corrupti. Sunt vero sit nobis veritatis? Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Laudantium qui neque quia culpa
            pariatur quis porro quod. Facere possimus eum illum? Quibusdam
            cumque labore saepe quia, explicabo non asperiores repudiandae.
          </p>
          <p className="card-text">
            <small className="text-muted">#SomosCarIV</small>
          </p>
        </div>
      </div>
    </div>
  );
}
