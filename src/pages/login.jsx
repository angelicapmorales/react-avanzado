import React, { useContext, useRef } from "react";
import LoginForm from "../components/login-form";
import { ThemeContext } from "../utils/theme.context";

const Login = () => {
  const ref = useRef(null);
  const context = useContext(ThemeContext);
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "tect-center";

  const login = (values) => {
    console.log(values);
  };

  let ds = false;
  if (ref.current) {
    ds = ref.current.isSubmitting;
  }

  return (
    <div className={"container bg-" + context.theme}>
      <div className="row">
        <div
          className="col-6"
          style={{
            backgroundSize: "cover",
            backgroundImage: "url(https://picsum.photos/800/300)",
          }}
        ></div>

        <div className="col-6 py-5">
          <h2 className={text_class}>LOGIN</h2>
          <LoginForm onSubmit={login} innerRef={ref} />
          <button
            className="btn btn-primary"
            disabled={ds}
            onClick={() => {
              if (ref.current) {
                ref.current.submitForm();
              }
              context.toggle_theme("dark");
            }}
          >
            Login
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
