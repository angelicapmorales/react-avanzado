import React, { useRef, useContext } from "react";
import ContactoForm from "../components/contact-form";
import { ThemeContext } from "../utils/theme.context";

const Contacto = () => {
  const ref = useRef(null);
  const contacto = (values) => {
    console.log(values);
  };
  const context = useContext(ThemeContext);

  return (
    <div className={"container bg-" + context.theme}>
      <div className="container py-5">
        <div className="row">
          <div className="col-3"></div>
          <div className="col-6">
            <ContactoForm onSubmit={contacto} innerRef={ref} />
            <button
              className="btn btn-primary"
              onClick={() => {
                if (ref.current) {
                  ref.current.submitForm();
                }
                context.toggle_theme("dark");
              }}
            >
              Enviar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contacto;
