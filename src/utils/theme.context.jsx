import React, { useState } from "react";

const inicial_data = {
  theme: "light",
};

export const ThemeContext = React.createContext(inicial_data);

const { Provider, Consumer } = ThemeContext;

const ThemeProvider = ({ _theme, children }) => {
  const [theme, set_theme] = useState(_theme);
  const toggle_theme = () => {
    set_theme((c) => (c === "light" ? "dark" : "light"));
  };
  return (
    <Provider
      value={{
        theme,
        toggle_theme,
      }}
    >
      {children}
    </Provider>
  );
};

export default ThemeProvider;
